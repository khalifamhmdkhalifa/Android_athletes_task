package com.example.khalifa.androidtask.Views;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khalifa.androidtask.Adapters.AtheletesAdapter;
import com.example.khalifa.androidtask.Entities.Athlete;
import com.example.khalifa.androidtask.Interfaces.AtheletesService;
import com.example.khalifa.androidtask.R;
import com.example.khalifa.androidtask.Views.fragments.AtheleteDetails;
import com.example.khalifa.androidtask.controllers.AthletesController;
import com.example.khalifa.androidtask.controllers.InternetHelper;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class MainActivity extends ActivityWithFragments
{
    Context context;

    List<Athlete> myAthletes;
    AtheletesAdapter atheletesAdapter;
    AthletesController athletesController;
    AtheletesReceiver atheletesReciever;
    @BindView(R.id.atheletesRV)RecyclerView atheletesRV;
//@BindView(R.id.frag_container) View frag_Container;
    @BindView(R.id.ActivityMain_LoadingView) View loadingView;
    @BindString(R.string.No_Internet_Message) String noInternetMessage;
    @BindString(R.string.FAILED_TO_CONNECT_TO_SERVER) String failedToConnectToServerMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        ButterKnife.bind(this);
        athletesController = new AthletesController();
        atheletesRV.setLayoutManager(new LinearLayoutManager(context));
if(InternetHelper.isInternetConnectionAvailable(this))
{
  loadData();
}else
    {
        loadingView.setVisibility(View.GONE);
        Toast.makeText(context,noInternetMessage,Toast.LENGTH_LONG).show();

    }

    }


void loadData()
{
loadingView.setVisibility(View.VISIBLE);
    if(atheletesReciever == null)
    atheletesReciever = new AtheletesReceiver();
    athletesController.getAtheletesList(atheletesReciever);
    atheletesRV.setAdapter(atheletesAdapter);

}

    @Override
    public int getDefaultFragmetContainerId() {
        return R.id.frag_container;
    }


    public class AtheletesReceiver  implements AthletesController.GetAtheleteListListener
{


    @Override
    public void onListRecieved(List<Athlete> atheletes)
    {
        loadingView.setVisibility(View.GONE);
        myAthletes  =  atheletes;
        atheletesAdapter = new AtheletesAdapter(myAthletes,context,new AtheletesAdapter.ItemClickListener()
        {

            @Override
            public void onItemClicked(int position, Athlete item) {
            replaceFragment(AtheleteDetails.newInstance(item),AtheleteDetails.class.getName(),true);

            }
        });
        atheletesRV.setAdapter(atheletesAdapter);
    }

    @Override
    public void onFailur(Throwable throwable) {
        loadingView.setVisibility(View.GONE);
        Toast.makeText(context,failedToConnectToServerMessage,Toast.LENGTH_LONG).show();
        throwable.printStackTrace();
        ((TextView)loadingView).setText(throwable.getMessage());
    }
}



}
