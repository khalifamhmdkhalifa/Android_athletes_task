package com.example.khalifa.androidtask.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.khalifa.androidtask.Entities.Athlete;
import com.example.khalifa.androidtask.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khalifa on 9/6/2017.
 */

public class AtheletesAdapter extends RecyclerView.Adapter {
    List<Athlete> atheletes;
    Context context;
    ItemClickListener itemClickListener;



    public AtheletesAdapter(List<Athlete> atheletes, Context context, ItemClickListener itemClickListener) {
        this.atheletes = atheletes;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomeViewHolder(LayoutInflater.from(context).inflate(R.layout.single_athelet_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        final Athlete athlete = atheletes.get(position);
        ((CustomeViewHolder)holder).root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemClickListener!=null)
                    itemClickListener.onItemClicked(position,athlete);
            }
        });

        ((CustomeViewHolder) holder).getName().setText(athlete.getName());

        Glide.with(context).load(athlete.getImage()).asBitmap().into(new BitmapImageViewTarget(((CustomeViewHolder) holder).getImageView())
        {
            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
            e.printStackTrace();
            }

        });

    }

    @Override
    public int getItemCount()
    {
        return atheletes.size();
    }



    class CustomeViewHolder extends RecyclerView.ViewHolder
    {

        //@BindView(R.id.single_athelete_item_image)
        ImageView imageView;
        //@BindView(R.id.single_athelete_item_name)
        TextView name;
        View root;

        public ImageView getImageView() {
            return imageView;
        }

        public void setImageView(ImageView imageView) {
            this.imageView = imageView;
        }

        public TextView getName() {
            return name;
        }

        public CustomeViewHolder(View itemView)
        {
            super(itemView);
            this.root = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.single_athelete_item_image);
            name = (TextView) itemView.findViewById(R.id.single_athelete_item_name);
            //ButterKnife.bind(itemView);
        }
    }


    public interface ItemClickListener
    {
     public void onItemClicked(int position,Athlete item);
    }
}
