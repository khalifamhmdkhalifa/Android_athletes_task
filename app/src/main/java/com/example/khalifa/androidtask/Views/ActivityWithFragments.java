package com.example.khalifa.androidtask.Views;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.example.khalifa.androidtask.R;

/**
 * Created by khalifa on 9/7/2017.
 */

public abstract class ActivityWithFragments extends FragmentActivity implements com.example.khalifa.androidtask.Interfaces.ActivityWithFragments
{


    @Override
    public void onBackPressed() {


        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() >= 1 )
        {
            fragmentManager.popBackStack();

        }else
        {
            super.onBackPressed();

        }
    }




    @Override
    public void replaceFragment(Fragment fragment, String tag, boolean saveTransaction)
    {

        if (saveTransaction) {
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(getDefaultFragmetContainerId(), fragment, tag)
                    .addToBackStack(tag)
                    .commit();
        } else {
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frag_container, fragment, tag)
                    .commit();

        }
    }

    @Override
    public void AddFragment(Fragment fragment, String tag,boolean saveTransaction)
    {

        if (saveTransaction) {
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(getDefaultFragmetContainerId(), fragment, tag).addToBackStack(tag)
                    .commit();
        } else
        {
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(getDefaultFragmetContainerId(), fragment, tag)
                    .commit();

        }
    }




    @Override
    public void removeFragmentFromBackStack(Fragment fragment)
    {
        if(fragment !=null)
        {
            FragmentManager manager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction trans = manager.beginTransaction();
            trans.commit();
            manager.popBackStack();
        }
    }


public abstract int getDefaultFragmetContainerId();

}
