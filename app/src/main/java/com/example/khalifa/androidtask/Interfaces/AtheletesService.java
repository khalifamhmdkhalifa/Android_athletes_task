package com.example.khalifa.androidtask.Interfaces;

import com.example.khalifa.androidtask.Entities.Athlete;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by khalifa on 9/6/2017.
 */

public interface AtheletesService
{
    @GET("https://gist.githubusercontent.com/Bassem-Samy/f227855df4d197d3737c304e9377c4d4/raw/ece2a30b16a77ee58091886bf6d3445946e10a23/athletes.josn")
    Call<GetAtheletesResponse> getAtheletesList();


    public class GetAtheletesResponse
    {
        public List<Athlete> getAthletes() {
            return athletes;
        }

        public void setAthletes(List<Athlete> athletes) {
            this.athletes = athletes;
        }

        List<Athlete> athletes;
    }
}
