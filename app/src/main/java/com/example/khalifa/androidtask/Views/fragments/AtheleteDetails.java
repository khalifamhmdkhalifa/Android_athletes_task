package com.example.khalifa.androidtask.Views.fragments;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.khalifa.androidtask.Adapters.AtheletesAdapter;
import com.example.khalifa.androidtask.Entities.Athlete;
import com.example.khalifa.androidtask.R;
import com.example.khalifa.androidtask.controllers.InternetHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AtheleteDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AtheleteDetails extends Fragment {
    private static final String ARG_PARAM1 = "param1";


    // TODO: Rename and change types of parameters
    private Athlete athlete;
    @BindView(R.id.fragment_athelete_details_name) TextView name;
    @BindView(R.id.fragment_athelete_details_brief) TextView brief;
    @BindView(R.id.fragment_athelete_details_image) ImageView image;




    public static AtheleteDetails newInstance(Athlete param1) {
        AtheleteDetails fragment = new AtheleteDetails();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            athlete = (Athlete) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  view = inflater.inflate(R.layout.fragment_athelete_details, container, false);
        ButterKnife.bind(this,view);
        if(InternetHelper.isInternetConnectionAvailable(getContext()))
        Glide.with(this).load(athlete.getImage()).asBitmap().into(new BitmapImageViewTarget(image)
        {
            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                e.printStackTrace();
            }

        });

        name.setText(athlete.getName());
        brief.setText(athlete.getBrief());
        return view;
    }

}
