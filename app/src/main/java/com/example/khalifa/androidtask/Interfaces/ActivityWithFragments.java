package com.example.khalifa.androidtask.Interfaces;


import android.support.v4.app.Fragment;

/**
 * Created by khalifa on 9/7/2017.
 */

public interface ActivityWithFragments
{
    public void replaceFragment(Fragment fragment, String newTag, boolean saveTransaction);

    public void AddFragment(Fragment fragment, String tag, boolean saveTransaction);

    public void removeFragmentFromBackStack(Fragment fragment);



}
