package com.example.khalifa.androidtask.controllers;

import com.example.khalifa.androidtask.Entities.Athlete;
import com.example.khalifa.androidtask.Interfaces.AtheletesService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by khalifa on 9/6/2017.
 */

public class AthletesController {


public Call getAtheletesList(final GetAtheleteListListener getAtheleteListListener)
{
   Retrofit retrofit =  HttpController.getDefaultBuilder();
    AtheletesService atheletesService = retrofit.create(AtheletesService.class);
    Call<AtheletesService.GetAtheletesResponse> call  = atheletesService.getAtheletesList();

    call.enqueue(new Callback<AtheletesService.GetAtheletesResponse>() {
        @Override
        public void onResponse(Call<AtheletesService.GetAtheletesResponse> call, Response<AtheletesService.GetAtheletesResponse> response) {
            getAtheleteListListener.onListRecieved(response.body().getAthletes());
        }

        @Override
        public void onFailure(Call<AtheletesService.GetAtheletesResponse> call, Throwable t) {
            getAtheleteListListener.onFailur(t);
        }
    });

return  call;
}


//view shouldn't use retrofit callback
public  interface GetAtheleteListListener
{
     void onListRecieved(List<Athlete> atheletes);
     void onFailur(Throwable throwable);

}



}
