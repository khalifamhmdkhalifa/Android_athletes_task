package com.example.khalifa.androidtask.controllers;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.khalifa.androidtask.Entities.Constants.URLS.BASE_URL;

/**
 * Created by khalifa on 9/6/2017.
 */

public class HttpController {



public static Retrofit getDefaultBuilder()
{

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return retrofit;

}


}
