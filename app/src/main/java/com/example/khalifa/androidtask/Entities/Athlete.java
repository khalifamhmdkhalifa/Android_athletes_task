package com.example.khalifa.androidtask.Entities;

import java.io.Serializable;

/**
 * Created by khalifa on 9/6/2017.
 */

public class Athlete implements Serializable
{

        String name;
    String image;
    String brief;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }
}
